<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller {

	public function __construct(){
		parent::__construct();
	}

	/**
     * Carga la vista de inicio
     * @author Jonatan Alexander Caicedo Caicedo <jonatan.cotacio@gmail.com>
     * @access public
     * @return view
     */
    public function index() {
        $this->load->view('template', array(
            'viewBody' => 'home/index'
        ));
    }

}
