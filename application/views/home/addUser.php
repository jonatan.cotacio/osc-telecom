<div class="modal fade" id="addUserForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Creación de usuario</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form id="formAddUser" v-on:submit="createUser($event)" autocomplete="off">
				<div class="modal-body">
					<div class="row">
						<div class="col-12">
							<div class="form-group">
								<label for="name">Nombre de usuario</label>
								<input type="text" maxlength="50" class="form-control" id="name" name="name" v-model="addUser.name" require>
							</div>
						</div>
						<div class="col-12"  v-if="addUser.id == ''">
							<div class="form-group">
								<label for="document">Documento</label>
								<input type="number" class="form-control" id="document" name="" v-model="addUser.document" v-on:keyup="addUser.user = addUser.document" require>
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<label for="email">Correo</label>
								<input type="email" class="form-control" id="email" name="email" v-model="addUser.email">
							</div>
						</div>
						<div class="col-12" v-if="addUser.id == ''">
							<div class="form-group">
								<label for="user">Usuario</label>
								<input type="text" maxlength="20" class="form-control" id="user" name="user" v-model="addUser.user" require>
							</div>
						</div>
						<div class="col-12" v-if="addUser.id == ''">
							<div class="form-group">
								<label for="password">Contraseña</label>
								<input type="password" maxlength="20" class="form-control" id="password" name="password" v-model="addUser.password" autocomplete="off" require>
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<label for="profile">Perfil</label>
								<select class="form-control" id="profile" name="profile" v-model="addUser.profile" require>
									<option value="">Seleccione un perfil...</option>
									<option v-for="d in profileData" v-bind:value="d.id">{{d.description}}</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-primary" v-if="addUser.id == ''"><i class="fa fa-save"></i> Crear</button>
					<button type="submit" class="btn btn-primary" v-if="addUser.id != ''"><i class="fa fa-edit"></i> Editar</button>
				</div>
			</form>
		</div>
	</div>
</div>
