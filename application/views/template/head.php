<head>
    <!--CSS-->
    <link href="<?= base_url('resources/css/styles/style.css?c=' . VAR_CACHE) ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('resources/css/bootstrap/bootstrap.min.css') ?>" type="text/css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    
    <!--JS-->
    <script src="<?= base_url('resources/js/jquery-3.5.1.min.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('resources/js/bootstrap/bootstrap.min.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url("node_modules/vue/dist/vue.js"); ?>" type="text/javascript"></script>
    <script src="<?= base_url("node_modules/vue-resource/dist/vue-resource.min.js"); ?>" type="text/javascript"></script>
    <script src="<?= base_url("node_modules/axios/dist/axios.js"); ?>" type="text/javascript"></script>
	
	<title>OSC Telecom</title>
</head>
