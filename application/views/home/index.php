<div class="container-fluid" id="listUser">
	<div class="row">
		<div class="col-8">
			<h2>Lista de usuarios</h2>
		</div>
		<div class="col-4">
			<button class="btn btn-primary btn-sm" v-on:click="openModal()"><i class="fa fa-user"></i> Crear usuarios</button>
			<a class="btn btn-success btn-sm" href="<?= base_url('UserController/excelUsersProcess') ?>"> <i class="fa fa-download"></i> Descargar usuarios</a>
		</div>
	</div>
	<div class="row mt-2">
		<div class="col-12"  v-show="data.length > 0">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>USUARIO</th>
						<th>NOMBRE USUARIO</th>
						<th>DOCUMENTO</th>
						<th>CONTRASEÑA</th>
						<th>PERFIL</th>
						<th>CORREO</th>
						<th>OPCIONES</th>
					</tr>
				</thead>
				<tbody>
					<tr v-for="d in data">
						<td>{{d.username}}</td>
						<td>{{d.name}}</td>
						<td>{{d.document}}</td>
						<td>{{d.password}}</td>
						<td>{{d.description}}</td>
						<td>{{d.email}}</td>
						<td>
							<button v-on:click="editUserModal(d)" class="btn btn-primary btn-sm text-light"><i class="fa fa-edit"></i> Editar</button>
							<button v-on:click="deleteUser(d.id)" class="btn btn-danger btn-sm text-light"><i class="fa fa-trash"></i> Eliminar</button>
							<button v-on:click="resetPass(d.id)" class="btn btn-dark btn-sm text-light"><i class="fa fa-unlock-alt"></i> Restaurar Contraseña</button>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<?php $this->load->view('home/addUser') ?>
</div>
<script src="<?= base_url('resources/js/controllers/userController.js?c=' . VAR_CACHE) ?>" type="text/javascript"></script>
