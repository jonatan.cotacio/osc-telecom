
var listUser = new Vue({
	el: "#listUser",
	data: {
		data: {},
		profileData: {},
		addUser: { id: '', user: '', name: '', document: '', profile: '', email: '', password: ''}
	},
	methods: {
		/**
		 * Carga la lista de usuarios
		 */
		getListUsers: function () {
			axios.post(BASE_URL + 'UserController/getUsersListProcess')
			.then((response) => {
				this.data = response.data.data;
			}).catch((err) => {
				alert(err.message)
			});
		},

		/**
		 * Consulta de perfiles
		 */
		getListProfiles: function () {
			axios.post(BASE_URL + 'UserController/getProfilesListProcess')
			.then((response) => {
				this.profileData = response.data.data;
			}).catch((err) => {
				alert(err.message)
			});
		},

		/**
		 * Abre el formulario de usuarios
		 */
		openModal: function() {
			this.addUser = { id: '', user: '', name: '', document: '', profile: '', email: '', password: ''};
			$("#addUserForm").modal('toggle');
		},

		/**
		 * Ejecuta la creación de usuarios
		 */
		createUser: function (e){
			e.preventDefault();

			var formdata = new URLSearchParams();
			formdata.append('id', this.addUser.id);
			formdata.append('user', this.addUser.user);
			formdata.append('name', this.addUser.name);
			formdata.append('document', this.addUser.document);
			formdata.append('profile', this.addUser.profile);
			formdata.append('email', this.addUser.email);
			formdata.append('password', this.addUser.password);

			let url = 'UserController/addUsersProcess';
			if(this.addUser.id != ''){url = 'UserController/updateUsersProcess';}

			axios.post(BASE_URL + url, formdata, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
			.then((response) => {
				if(response.data.status == 200){
					$("#addUserForm").modal('toggle');
					this.addUser = { id: '', user: '', name: '', document: '', profile: '', email: '', password: ''};
					this.getListUsers();
				}else{
					alert(response.data.message)
				}
			}).catch((err) => {
				 alert(err.data)
			});
		},

		/**
		 * Eliminar usuario
		 */
		deleteUser: function (id) {
			let confirm = window.confirm('¿Desea eliminar el usuario?');
			if(!confirm){return false;}

			var formdata = new URLSearchParams();
			formdata.append('id', id);
			axios.post(BASE_URL + 'UserController/deleteUsersProcess', formdata, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
			.then((response) => {
				if(response.data.status == 200){
					this.getListUsers();
				}else{
					alert(response.data.message)
				}
			}).catch((err) => {
				 alert(err.data)
			});
		},

		/**
		 * Actualiza la contraseña
		 */
		resetPass: function (id) {
			var formdata = new URLSearchParams();
			formdata.append('id', id);
			axios.post(BASE_URL + 'UserController/resetPasswordProcess', formdata, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
			.then((response) => {
				if(response.data.status == 200){
					alert(response.data.message);
					this.getListUsers();
				}else{
					alert(response.data.message);
				}
			}).catch((err) => {
				 alert(err.data)
			});
		},

		/**
		 * Abre el formulario de usuarios
		 */
		editUserModal: function(data) {
			this.addUser.id = data.id;
			this.addUser.name = data.name;
			this.addUser.document = data.document;
			this.addUser.profile = data.id_profile;
			this.addUser.email = data.email;
			this.addUser.password = data.password;
			this.addUser.user = data.username;
			$("#addUserForm").modal('toggle');
		},

	},
	mounted: function () {
		this.getListUsers();
		this.getListProfiles();
	}
});
