<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class LogUser extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->load->model('persistence/DbUser','dbUser');
	}

	/**
	 * Carga la lista de usuarios
	 * @return JSON
	 */
	public function listUsers(){
		$data = $this->dbUser->getUsersDb();
		return array('status' => 200, 'message' => 'Lista de usuarios', 'data' => $data);
	}

	/**
	 * Carga la lista de perfiles
	 * @return JSON
	 */
	public function listProfiles(){
		$data = $this->dbUser->listProfiles();
		return array('status' => 200, 'message' => 'Lista de usuarios', 'data' => $data);
	}

	/**
	 * Creación de usuario
	 */
	public function addUser($formData){
		$name = isset($formData['name']) ? trim($formData['name']) : '';
		$document = isset($formData['document']) ? trim($formData['document']) : '';
		$profile = isset($formData['profile']) ? trim($formData['profile']) : '';
		$email = isset($formData['email']) ? trim($formData['email']) : '';
		$password = isset($formData['password']) ? trim($formData['password']) : '';
		$user = isset($formData['user']) ? trim($formData['user']) : $document;

		if(!$name){return array('status' => 400, 'message' => 'El nombre del usuario es obligatorio');}
		if(!$document){return array('status' => 400, 'message' => 'El documento del usuario es obligatorio');}
		if(!$profile){return array('status' => 400, 'message' => 'El perfil del usuario es obligatorio');}
		if(!$password){return array('status' => 400, 'message' => 'La contraseña es obligatoria');}

		$data = array(
			'username' => $user,
			'name' => $name,
			'document' => $document,
			'password' => sha1($password),
			'email' => $email,
			'id_profile' => $profile,
		);

		$id = $this->dbUser->saveUser($data);
		if(!$id){throw new Exception('Ocurrio un error al crear el usuario');}

		return array('status' => 200, 'message' => 'Se ha creado correctamente el usuario');
	}

	/**
	 * Actualización de usuario
	 */
	public function updateUsers($formData){
		$id = isset($formData['id']) ? trim($formData['id']) : '';
		$name = isset($formData['name']) ? trim($formData['name']) : '';
		$document = isset($formData['document']) ? trim($formData['document']) : '';
		$profile = isset($formData['profile']) ? trim($formData['profile']) : '';
		$email = isset($formData['email']) ? trim($formData['email']) : '';
		$user = isset($formData['user']) ? trim($formData['user']) : $document;

		if(!$id){return array('status' => 400, 'message' => 'El Id del usuario es obligatorio');}
		if(!$name){return array('status' => 400, 'message' => 'El nombre del usuario es obligatorio');}
		if(!$document){return array('status' => 400, 'message' => 'El documento del usuario es obligatorio');}
		if(!$profile){return array('status' => 400, 'message' => 'El perfil del usuario es obligatorio');}

		$set = array(
			'username' => $user,
			'name' => $name,
			'document' => $document,
			'email' => $email,
			'id_profile' => $profile,
		);

		$where = array('id' => $id);
		$update = $this->dbUser->UpdateUser($set, $where);
		if(!$update){throw new Exception('Ocurrio un error al actualizar el usuario');}

		return array('status' => 200, 'message' => 'Se ha actualizado correctamente el usuario');
	}

	/**
	 * Eliminación logica de usuarios
	 */
	public function deleteUser($formData){
		$id = isset($formData['id']) ? trim($formData['id']) : '';
		if(!$id){return array('status' => 400, 'message' => 'El Id del usuario es obligatorio');}

		$set = array('status' => 0);
		$where = array('id' => $id);
		$update = $this->dbUser->UpdateUser($set, $where);
		if(!$update){throw new Exception('Ocurrio un error al actualizar el usuario');}

		return array('status' => 200, 'message' => 'Se ha eliminado correctamente el usuario');
	}

	/**
	 * Eliminación logica de usuarios
	 */
	public function resetPassword($formData){
		$id = isset($formData['id']) ? trim($formData['id']) : '';
		if(!$id){return array('status' => 400, 'message' => 'El Id del usuario es obligatorio');}

		$password = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);

		$set = array('password' => sha1($password));
		$where = array('id' => $id);
		$update = $this->dbUser->UpdateUser($set, $where);
		if(!$update){throw new Exception('Ocurrio un error al actualizar el usuario');}

		return array('status' => 200, 'message' => 'La nueva contraseña es: ' . $password);
	}

	/**
	 * Descarga el excel
	 */
	public function excelUsers() {
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'USUARIO');
		$sheet->setCellValue('B1', 'NOMBRE USUARIO');
		$sheet->setCellValue('C1', 'DOCUMENTO');
		$sheet->setCellValue('D1', 'CONTRASEÑA');
		$sheet->setCellValue('E1', 'PERFIL');
		$sheet->setCellValue('F1', 'CORREO');
		
		$data = $this->dbUser->getUsersDb();
		foreach ($data as $key => $value) {
			$sheet->setCellValue('A'. ($key+2), $value['username']);
			$sheet->setCellValue('B'. ($key+2), $value['name']);
			$sheet->setCellValue('C'. ($key+2), $value['document']);
			$sheet->setCellValue('D'. ($key+2), $value['password']);
			$sheet->setCellValue('E'. ($key+2), $value['description']);
			$sheet->setCellValue('F'. ($key+2), $value['email']);
		}

		$sheet->getStyle('A1:F1')->getFont()->setBold(true);

		$writer = new Xlsx($spreadsheet);
		
		$filename = 'lISTA DE USUARIOS';
		
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
		header('Cache-Control: max-age=0');
		
		$writer->save('php://output');
	}
}
