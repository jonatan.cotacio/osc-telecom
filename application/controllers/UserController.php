<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('business/LogUser', 'logUser');
	}

	/**
	 * Procesa la carga de la lista de usuarios
	 * @author Jonatan Alexander Caicedo Caicedo <jonatan.cotacio@gmail.com>
     * @access public
	 * @return JSON
	 */
	public function getUsersListProcess (){
		try {
            $response = $this->logUser->listUsers();
            header('Content-type: application/json; charset=utf-8');
            echo json_encode($response);
        } catch (Exception $e) {
            echo json_encode(array(
                'status' => 400,
                'message' => 'Ocurrio un error por favor comuniquese con el administrador',
                'data' => $e->getMessage()
            ));
        }
	}

	/**
	 * Procesa la carga de la lista de perfiles
	 * @author Jonatan Alexander Caicedo Caicedo <jonatan.cotacio@gmail.com>
     * @access public
	 * @return JSON
	 */
	public function getProfilesListProcess (){
		try {
            $response = $this->logUser->listProfiles();
            header('Content-type: application/json; charset=utf-8');
            echo json_encode($response);
        } catch (Exception $e) {
            echo json_encode(array(
                'status' => 400,
                'message' => 'Ocurrio un error por favor comuniquese con el administrador',
                'data' => $e->getMessage()
            ));
        }
	}

	/**
	 * Procesa la creación de usuarios
	 * @author Jonatan Alexander Caicedo Caicedo <jonatan.cotacio@gmail.com>
     * @access public
	 * @return JSON
	 */
	public function addUsersProcess (){
		try {
			$formData = $this->input->post();
            $response = $this->logUser->addUser($formData);
            header('Content-type: application/json; charset=utf-8');
            echo json_encode($response);
        } catch (Exception $e) {
            echo json_encode(array(
                'status' => 400,
                'message' => 'Ocurrio un error por favor comuniquese con el administrador',
                'data' => $e->getMessage()
            ));
        }
	}

	/**
	 * Procesa la eliminación de usuarios
	 * @author Jonatan Alexander Caicedo Caicedo <jonatan.cotacio@gmail.com>
     * @access public
	 * @return JSON
	 */
	public function deleteUsersProcess (){
		try {
			$formData = $this->input->post();
            $response = $this->logUser->deleteUser($formData);
            header('Content-type: application/json; charset=utf-8');
            echo json_encode($response);
        } catch (Exception $e) {
            echo json_encode(array(
                'status' => 400,
                'message' => 'Ocurrio un error por favor comuniquese con el administrador',
                'data' => $e->getMessage()
            ));
        }
	}

	/**
	 * Procesa la actualización de la contraseña
	 * @author Jonatan Alexander Caicedo Caicedo <jonatan.cotacio@gmail.com>
     * @access public
	 * @return JSON
	 */
	public function resetPasswordProcess (){
		try {
			$formData = $this->input->post();
            $response = $this->logUser->resetPassword($formData);
            header('Content-type: application/json; charset=utf-8');
            echo json_encode($response);
        } catch (Exception $e) {
            echo json_encode(array(
                'status' => 400,
                'message' => 'Ocurrio un error por favor comuniquese con el administrador',
                'data' => $e->getMessage()
            ));
        }
	}

	/**
	 * Procesa la actualización del usuario
	 * @author Jonatan Alexander Caicedo Caicedo <jonatan.cotacio@gmail.com>
     * @access public
	 * @return JSON
	 */
	public function updateUsersProcess (){
		try {
			$formData = $this->input->post();
            $response = $this->logUser->updateUsers($formData);
            header('Content-type: application/json; charset=utf-8');
            echo json_encode($response);
        } catch (Exception $e) {
            echo json_encode(array(
                'status' => 400,
                'message' => 'Ocurrio un error por favor comuniquese con el administrador',
                'data' => $e->getMessage()
            ));
        }
	}

	/**
	 * Procesa la descarga del excel
	 * @author Jonatan Alexander Caicedo Caicedo <jonatan.cotacio@gmail.com>
     * @access public
	 * @return JSON
	 */
	public function excelUsersProcess (){
		try {
            $response = $this->logUser->excelUsers();
            header('Content-type: application/json; charset=utf-8');
            echo json_encode($response);
        } catch (Exception $e) {
            echo json_encode(array(
                'status' => 400,
                'message' => 'Ocurrio un error por favor comuniquese con el administrador',
                'data' => $e->getMessage()
            ));
        }
	}

}
