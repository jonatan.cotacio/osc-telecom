<?php

class DbUser extends CI_Model {

	public function __construct(){
		parent::__construct();
	}

	/**
	 * Consulta la lista de usuarios
	 */
	public function getUsersDb() {
		$query = $this->db->select('u.*, p.description')
						->join('profiles AS p', 'p.id = u.id_profile')
						->where('u.status', 1)
						->get('users AS u');
		$error = $this->db->error();
		if ($error['message']) {throw new Exception($error['message'] . ' class' . __CLASS__ . ' line' . __LINE__);}
		return $query->result_array();
	}

	/**
	 * Consulta la lista de usuarios
	 */
	public function listProfiles() {
		$query = $this->db->select('*')->get('profiles');
		$error = $this->db->error();
		if ($error['message']) {throw new Exception($error['message'] . ' class' . __CLASS__ . ' line' . __LINE__);}
		return $query->result_array();
	}

	/**
	 * Inserta los usuarios
	 */
	public function saveUser($data) {
		$this->db->insert('users', $data);
		return $this->db->insert_id();
	}

	/**
	 * Actualización de usuario
	 */
	public function UpdateUser($set, $where) {
		$this->db->set($set);
		$this->db->where($where);
		$this->db->update('users');
		return $this->db->affected_rows();
	}
}
